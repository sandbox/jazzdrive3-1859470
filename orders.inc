<?php

/**
 * @file
 *   Commerce Order migration.
 */

class CommerceMigrateOSCOrderMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import orders from osCommerce.');
    $this->softDependencies = array('CommerceMigrateOSCCustomerBillingProfile');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'orders_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Ubercart order ID',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_order', 'commerce_order')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();

    $query = $connection->select('orders', 'o');
    $query->leftJoin('customers', 'c', 'c.customers_id = o.customers_id');
    $query->leftJoin('orders_total', 'ot', 'ot.orders_id = o.orders_id');
    $query->leftJoin('orders_status', 'os', 'os.orders_status_id = o.orders_status');
    $query->fields('o', array('orders_id', 'customers_id', 'last_modified', 'date_purchased'));
    $query->fields('c', array('customers_email_address'));
    $query->fields('ot', array('value'));
    $query->fields('os', array('orders_status_id'));
    $query->condition('ot.class', 'ot_total', '=');

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_order', 'commerce_order');

    // Field Mappings
    $this->addFieldMapping('order_number', 'orders_id');
    $this->addFieldMapping('mail', 'customers_email_address');  
    $this->addFieldMapping('commerce_order_total', 'value');
    
  }

  function prepare($order, stdClass $row) {
  
    // Set the status for the order.
    // This is based on the default osCommerce statuses. You can find status IDs
    // for your osCommerce installation in the orders_status table
    if ($row->orders_status_id == 1) {
      $order->status = 'pending';
    } 
    else if ($row->orders_status_id == 2 || $row->orders_status_id == 5) {
      $order->status = 'processing';
    }
    else if ($row->orders_status_id == 3 || $row->orders_status_id == 4) {
      $order->status = 'completed';
    }
    
    if ($uid = commerce_migrate_osc_get_uid_from_email($row->customers_email_address)) {
      $order->uid = $uid;
    }
    
    // Attach a billing profile to the order. We essentially just choose the last billing profile the customer has.
    
    $profiles = commerce_customer_profile_load_multiple(FALSE, array('uid' => $uid));
    
    foreach ($profiles as $profile_id => $profile) {
        $order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'] = $profile_id;
    }
    
    $order->created = strtotime($row->date_purchased);
    $order->changed = strtotime($row->last_modified);
    
    
  }

  /**
   * The line item controller kills the order_total we set.
   * Until that gets fixed, here's a workaround.
   */
  function complete($order, stdClass $row) {
    db_update('field_data_commerce_order_total')
      ->fields(array(
        'commerce_order_total_amount' => commerce_currency_decimal_to_amount($row->value, commerce_default_currency()),
      ))
      ->condition('entity_id', $order->order_id)
      ->condition('entity_type', 'commerce_order')
      ->execute();
  }
}
