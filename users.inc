<?php

/**
 * @file
 *   osCommerce User account migration.
 *   Create Drupal accounts based on the email addresses in
 *   the osCommerce customers table.
 */

class CommerceMigrateOSCUserMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import user accounts from osCommerce. Original passwords will not be preserved. Users will need to request new passwords.');
    
    // Set up desination and source
    $connection = commerce_migrate_osc_get_source_connection();
    $query = $connection->select('customers', 'c');
    $query->fields('c', array('customers_firstname', 'customers_lastname', 'customers_email_address', 'customers_password'));
    
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationUser();
    
    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'customers_email_address' => array(
            'type' => 'varchar',
            'not null' => FALSE,
            'length' => 96,
            'description' => 'Customer email address',
          ),
        ),
        array(
          'mail' => array(
            'type' => 'varchar',
            'not null' => FALSE,
            'length' => 254,
            'description' => 'Drupal account email address',
          ),
        )
      );

      
    // Set up field mappings
    
    $this->addFieldMapping('mail', 'customers_email_address');
    $this->addFieldMapping('pass', 'customers_password');
    $this->addFieldMapping('init', 'customers_email_address');
    
    // We set the username to their email address, as that is the only way to ensure uniqueness.
    
    $this->addFieldMapping('name', 'customers_email_address');
    
  }
  
  public function prepare($user, stdClass $row) {
    //We need to add the field info to the User object.
    
    // If we are putting customer's names on the user account itself, get the field name and add the mapping.
    if (variable_get('commerce_migrate_osc_names', FALSE)) {
      $first_name_field = variable_get('commerce_migrate_osc_first_name');
      $last_name_field = variable_get('commerce_migrate_osc_last_name');
      $user->{$first_name_field}[LANGUAGE_NONE][0]['value'] = $row->customers_firstname;
      $user->{$last_name_field}[LANGUAGE_NONE][0]['value'] = $row->customers_lastname;
    }
    $user->status = 1;
  }
}