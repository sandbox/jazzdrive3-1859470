<?php

/**
 * @file
 *   Commerce Line Item migration.
 *
 *   @todo Do we need to fill out the serialized data column as well?
 *         It holds the path to the product node and an entity object,
 *         not sure how important it is.
 */

class CommerceMigrateOSCLineItem extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import order line items from osCommerce.');
    $this->softDependencies = array('CommerceMigrateOSCOrder');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'orders_products_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'osCommerce order product ID',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_line_item', 'product')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();

    $query = $connection->select('orders_products', 'op');
    $query->fields('op', array('orders_products_id', 'orders_id', 'products_id', 'products_model',
                  'products_quantity', 'products_price', 'products_name'));
                  
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_line_item', 'product');


    $this->addFieldMapping('order_id', 'orders_id')
         ->sourceMigration('CommerceMigrateOSCOrder');
    $this->addFieldMapping('line_item_label', 'products_model');
    $this->addFieldMapping('quantity', 'products_quantity');
    $this->addFieldMapping('commerce_unit_price', 'products_price');
    $this->addFieldMapping('commerce_total', 'products_price');
    $this->addFieldMapping('commerce_product', 'products_id')
      ->defaultValue(0)
      ->sourceMigration('CommerceMigrateOSCProduct');
      
    // @todo
    //$this->addFieldMapping('commerce_display_path', '');
  }

  function prepare($line_item, stdClass $row) {
    $line_item->type = 'product';
    $line_item->line_item_label =  $row->products_name . ' SKU:' . $line_item->line_item_label;
  }

  /**
   * A line item has been saved. Reference it from the order.
   */
  function complete($line_item, stdClass $row) {
    $order_id = $line_item->order_id['destid1'];
    $delta = db_query("SELECT COUNT(entity_id) FROM {field_data_commerce_line_items} WHERE entity_id = :order_id",
                      array(':order_id' => $order_id))->fetchField();

    db_insert('field_data_commerce_line_items')
      ->fields(array(
        'entity_id' => $order_id,
        'revision_id' => $order_id,
        'entity_type' => 'commerce_order',
        'bundle' => 'commerce_order',
        'deleted' => 0,
        'language' => LANGUAGE_NONE,
        'delta' => $delta,
        'commerce_line_items_line_item_id' => $line_item->line_item_id,
      ))
      ->execute();
  }

  /**
   * The line item has been deleted, delete its references.
   */
  function completeRollback($line_item_id) {
    db_delete('field_data_commerce_line_items')
      ->condition('commerce_line_items_line_item_id', $line_item_id)
      ->execute();
  }

}
