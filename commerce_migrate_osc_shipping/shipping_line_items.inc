<?php

/**
 * @file
 *   Commerce Shipping Line Item migration from osCommerce.
 */

class CommerceMigrateOSCShippingLineItemMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import shipping line items from osCommerce.');
    $this->softDependencies = array('CommerceMigrateOSCOrder', 'CommerceMigrateOSCLineItem');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'orders_total_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'osCommerce order totals ID',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_line_item', 'shipping')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();

    
    // We need to pull from the orders table to get shipping information.
    $query = $connection->select('orders', 'o');
    $query->leftJoin('orders_total', 'ot', 'ot.orders_id = o.orders_id');
    $query->fields('o', array('orders_id', 'last_modified', 'date_purchased'));
    $query->fields('ot', array('value', 'title', 'orders_total_id'));
    $query->condition('ot.class', 'ot_shipping', '=');

    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_line_item', 'shipping');

    // Field Mappings
    $this->addFieldMapping('line_item_label', 'title');
    $this->addFieldMapping('quantity')->defaultValue(1); 
    $this->addFieldMapping('commerce_unit_price', 'value');
    $this->addFieldMapping('commerce_total', 'value');
    
  }
  
  function prepare($line_item, stdClass $row) {
    $line_item->type = 'shipping';
    $line_item->created = strtotime($row->date_purchased);
    
    $order = commerce_order_load_by_number($row->orders_id);
    
    $line_item->order_id = $order->order_id;
    
    $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
    $line_item_wrapper->commerce_unit_price->data = commerce_price_component_add(
      $line_item_wrapper->commerce_unit_price->value(),
      'shipping',
      array(
          'amount' => $line_item_wrapper->commerce_unit_price->amount->value(),
          'currency_code' => $line_item_wrapper->commerce_unit_price->currency_code->value(),
          'data' => array(),
        ),
        TRUE
      );
      
  }
  
  /**
   * A line item has been saved. Reference it from the order.
   */
  function complete($line_item, stdClass $row) {
    $order_id = $line_item->order_id;
    $delta = db_query("SELECT COUNT(entity_id) FROM {field_data_commerce_line_items} WHERE entity_id = :order_id",
                      array(':order_id' => $order_id))->fetchField();

    db_insert('field_data_commerce_line_items')
      ->fields(array(
        'entity_id' => $order_id,
        'revision_id' => $order_id,
        'entity_type' => 'commerce_order',
        'bundle' => 'commerce_order',
        'deleted' => 0,
        'language' => LANGUAGE_NONE,
        'delta' => $delta,
        'commerce_line_items_line_item_id' => $line_item->line_item_id,
      ))
      ->execute();
  }

  /**
   * The line item has been deleted, delete its references.
   */
  function completeRollback($line_item_id) {
    db_delete('field_data_commerce_line_items')
      ->condition('commerce_line_items_line_item_id', $line_item_id)
      ->execute();
  }
}
