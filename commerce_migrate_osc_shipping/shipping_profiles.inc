<?php

/**
 * @file
 *   Create customer shipping profiles from osCommerce addresses from orders.
 */

class CommerceMigrateOSCShippingProfileMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import customer shipping profiles from osCommerce.');
    $this->softDependencies = array('CommerceMigrateOSCUser', 'CommerceMigrateOSCOrder');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'orders_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Order ID',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_customer_profile', 'shipping')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();

    $query = $connection->select('orders', 'o');
    $query->leftJoin('customers', 'c', 'c.customers_id = o.customers_id');
    $query->leftJoin('countries', 'co', 'co.countries_name = o.delivery_country');
    $query->leftJoin('zones', 'z', 'z.zone_name = o.delivery_state');
    $query->fields('o', array('orders_id', 'customers_id', 'last_modified', 'date_purchased', 'delivery_name', 'delivery_company',
                  'delivery_street_address', 'delivery_city', 'delivery_postcode', 'delivery_state', 'delivery_country'));
    $query->fields('c', array('customers_email_address'));
    $query->fields('co', array('countries_iso_code_2'));
    $query->fields('z', array('zone_code'));


    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_customer_profile', 'shipping');
    
    
  }

  public function prepare($profile, stdClass $row) {
    $profile->type = 'shipping';
    $profile->status = TRUE;

    $name = $row->delivery_name;
    
    // There are too many fields to do this through a field handler.
    // @todo Not comfortable with assuming LANGUAGE_NONE.
    $profile->commerce_customer_address[LANGUAGE_NONE][0] = array(
        'xnl' => '<NameDetails PartyType="Person"><NameLine>' . $name . '</NameLine></NameDetails>',
        'name_line' => $name,
        'administrative_area' => $row->zone_code,
        'country' => $row->countries_iso_code_2,
        'thoroughfare' => $row->delivery_street_address,
        'locality' => $row->delivery_city,
        'postal_code' => $row->delivery_postcode,
        'organisation_name' => $row->delivery_company,
      );
      
    // Get the uid of the owner of the address, if they exist.    
    if ($uid = commerce_migrate_osc_get_uid_from_email($row->customers_email_address)) {
      $profile->uid = $uid;
    }
    
    //convert times to unix timestamp
    $profile->created = strtotime($row->date_purchased);
    $profile->changed = strtotime($row->last_modified);
    
  }
  
  /**
   * The shipping profile has been deleted, so delete its references.
   */
  
  function completeRollback($line_item_id) {
    db_delete('field_data_commerce_customer_shipping')
      ->condition('commerce_customer_shipping_profile_id', $line_item_id)
      ->execute();
  }
}
