<?php

/**
 * @file
 *   osCommerce Address Book Migration
 *   Create customer billing profiles from osCommerce addresses.
 */

class CommerceMigrateOSCCustomerBillingProfileMigration extends Migration {
  public function __construct() {
    parent::__construct();
    $this->description = t('Import customer billing profiles from osCommerce.');
    $this->softDependencies = array('CommerceMigrateOSCUser');

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
        array(
          'address_book_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Address Book ID',
          ),
        ),
        MigrateDestinationEntityAPI::getKeySchema('commerce_customer_profile', 'billing')
      );

    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();

    $query = $connection->select('address_book', 'ab');
    $query->leftJoin('customers', 'c', 'c.customers_id = ab.customers_id');
    $query->leftJoin('countries', 'cou', 'cou.countries_id = ab.entry_country_id');
    $query->leftJoin('zones', 'z', 'z.zone_id = ab.entry_zone_id');
    $query->fields('ab', array('address_book_id', 'entry_firstname', 'entry_lastname',
                                'entry_street_address', 'entry_postcode',
                                'entry_city', 'entry_state', 'entry_company'));
    $query->fields('cou', array('countries_iso_code_2'));
    $query->fields('c', array('customers_email_address', ));
    $query->fields('z', array('zone_code'));


    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));
    $this->destination = new MigrateDestinationEntityAPI('commerce_customer_profile', 'billing');
    
  }

  public function prepare($profile, stdClass $row) {
    $profile->type = 'billing';
    $profile->status = TRUE;

    $name = $row->entry_firstname . ' ' . $row->entry_lastname;
    
    // There are too many fields to do this through a field handler.
    // @todo Not comfortable with assuming LANGUAGE_NONE.
    $profile->commerce_customer_address[LANGUAGE_NONE][0] = array(
        'xnl' => '<NameDetails PartyType="Person"><NameLine>' . $name . '</NameLine></NameDetails>',
        'first_name' => $row->entry_firstname,
        'last_name' => $row->entry_lastname,
        'name_line' => $name,
        'administrative_area' => $row->zone_code,
        'country' => $row->countries_iso_code_2,
        'thoroughfare' => $row->entry_street_address,
        'locality' => $row->entry_city,
        'postal_code' => $row->entry_postcode,
        'organisation_name' => $row->entry_company,
      );
      
    // Get the uid of the owner of the address, if they exist.    
    if ($uid = commerce_migrate_osc_get_uid_from_email($row->customers_email_address)) {
      $profile->uid = $uid;
    }
    
    // Assign the phone number to the appropriate field, if one has been selected in the settings.
    if($field_name = variable_get('commerce_migrate_osc_phone_field', FALSE)) {
      $profile->{$field_name}[LANGUAGE_NONE][0]['value'] = $row->customers_telephone;
    }
  }
}
