SETUP
=================

You must add the source database in your settings.php file.
Copy the below code, changing it to suit your connection.
You must keep the first line the same.

$databases['commerce_migrate']['osc_source'] = array(
    'driver' => 'mysql',
    'database' => 'dbname',
    'username' => 'dbuser',
    'password' => '',
    'host' => '127.0.0.1',
    'port' => 33066 );
    
If this is not in your settings.php, the module will
assume the database tables are in your drupal default
database, which is probably not the case.