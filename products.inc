<?php

/**
 * @file
 *   Commerce Product migration from osCommerce.
 *   
 */

class CommerceMigrateOSCProductMigration extends Migration {

  public function __construct() {
    parent::__construct();
    $this->description = t('Import products from osCommerce.');
    $this->image_fields = preg_split('/ *[, ]+ */', variable_get('commerce_migrate_ubercart_image_fields', 'field_image_cache'));

    $this->file_public_path = variable_get('commerce_migrate_ubercart_public_files_directory', variable_get('file_public_path', 'sites/default/files'));
    $this->source_drupal_root = variable_get('commerce_migrate_ubercart_source_drupal_root', DRUPAL_ROOT);

    // Create a map object for tracking the relationships between source rows
    $this->map = new MigrateSQLMap($this->machineName,
      array(
          'products_id' => array(
            'type' => 'int',
            'unsigned' => TRUE,
            'not null' => TRUE,
            'description' => 'Ubercart node ID',
            'alias' => 'ucp',
          ),
      ),
      MigrateDestinationEntityAPI::getKeySchema('commerce_product', 'product')
    );
    // Create a MigrateSource object, which manages retrieving the input data.
    $connection = commerce_migrate_osc_get_source_connection();
    $query = $connection->select('products', 'p');
    $query->leftJoin('products_description', 'pd', 'pd.products_id = p.products_id');
    $query->fields('p', array('products_id', 'products_model', 'products_price', 'products_date_added', 'products_last_modified',
                  'products_personalize_image', 'products_weight', 'products_width', 'products_height', 'products_length', 'upc_code'))
          ->fields('pd', array('products_name', 'products_description'))
          ->distinct();

    
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->destination = new MigrateDestinationEntityAPI('commerce_product', 'product');

    // Properties
    $this->addFieldMapping('sku', 'products_model');
    $this->addFieldMapping('title', 'products_name');
    // Default uid to 0 if we're not mapping it.
    $this->addFieldMapping('uid')->defaultValue(0);
    $this->addFieldMapping('status')
      ->defaultValue(1);
    // Fields
    $this->addFieldMapping('commerce_price', 'products_price');
    
    // If we are putting descriptions on the product entities themselves, map the field.
    if (variable_get('commerce_migrate_osc_product_descriptions', FALSE)) {
      $field = variable_get('commerce_migrate_osc_product_desc_field', '');
      $this->addFieldMapping($field, 'products_description');
    }
    
    // If we are putting descriptions on the product entities themselves, mset the format for the field.
    if (variable_get('commerce_migrate_osc_product_descriptions', FALSE)) {
      $format = variable_get('commerce_migrate_osc_product_desc_format', '');
      $this->addFieldMapping('field_product_description:format')->defaultValue($format);
    }
  }

  function prepare($product, stdClass $row) {
    // Set dates to Unix timestamps
    //$product->created = strtotime($row->products_date_added);
    //$product->changed = strtotime($row->products_last_modified);
    
    $length = str_replace('"', '', $row->products_length);
    $length = str_replace('1/2', '.5', $length);
    $length = str_replace('3/4', '.75', $length);
    $length = str_replace('1/4', '.25', $length);
    $length = str_replace(' ', '', $length);
    $length = str_replace('-', '', $length);
    $width = str_replace('"', '', $row->products_width);
    $width = str_replace('1/2', '.5', $width);
    $width = str_replace('3/4', '.75', $width);
    $width = str_replace('1/4', '.25', $width);
    $width = str_replace(' ', '', $width);
    $width = str_replace('-', '', $width);
    $height = str_replace('"', '', $row->products_height);
    $height = str_replace('1/2', '.5', $height);
    $height = str_replace('3/4', '.75', $height);
    $height = str_replace('1/4', '.25', $height);
    $height = str_replace(' ', '', $height);
    $height = str_replace('-', '', $height);
    
    // Set dimensions
    $dimensions = array(
      'length' => !empty($row->products_length) && $row->products_length != '' ? (float) $length : .25,
      'width' => !empty($row->products_width) && $row->products_width != '' ? (float) $width : .25,
      'height' => !empty($row->products_height) && $row->products_height != '' ? (float) $height : .25,
      'unit' => 'in',
    );
    
    $product->field_dimensions[LANGUAGE_NONE][0] = $dimensions;
    
    $product->field_weight[LANGUAGE_NONE][0] = array('weight' => $row->products_weight, 'unit' => 'lb');
    $product->field_upc_code[LANGUAGE_NONE][0]['value'] = $row->upc_code;
  }

  function complete($product, stdClass $row) {
  }
}